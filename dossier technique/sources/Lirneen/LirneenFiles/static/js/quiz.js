let quizNumber=0

function addquiz() {
    var quizContent = document.getElementById("quiz-container");
    var quizDiv = document.createElement("div");
    var quizNumber = quizContent.children.length + 1;
    quizDiv.className = `quiz${quizNumber}`;
    quizDiv.id = `quiz${quizNumber}`;

    quizDiv.innerHTML =
    `
    <div class="question-container">
        <form class="quiz-form" method="post">
        <label for="question1">Question:</label>
        <input type="text" id="question1" name="question1" placeholder="Entrez votre question..." required>
        <label for="answer1">Réponse 1:</label>
        <input type="text" id="answer1" name="answer1" placeholder="Entrez la réponse..." required>
        <label for="answer2">Réponse 2:</label>
        <input type="text" id="answer2" name="answer2" placeholder="Entrez la réponse..." required>
        <label for="answer3">Réponse 3:</label>
        <input type="text" id="answer3" name="answer3" placeholder="Entrez la réponse..." >
        <label for="answer4">Réponse 4:</label>
        <input type="text" id="answer4" name="answer4" placeholder="Entrez la réponse..." >
        
        <label for="choix_reponse">Quel est la réponse ?</label>
        <input type="number" id="br" name="br" min="1" max="4" required>
        <hr>
        <button type="submit">créer</button>
        </form>
    </div>
    
    <label for="choix_reponse">Quel est la réponse ?</label>
        <input type="number" id="tentacles" name="br" min="1" max="4" required>
        <br>
        <br>

        </div>
        <button type="button" class="removeBtn" onclick=deletequiz(${quizNumber})>Supprimer le quiz</button>
    `;

    quizContent.appendChild(quizDiv);
};
let quizListNumber = 0

function addquizList() {
    var quizContent = document.getElementById("quiz-contain");
    var quizDiv = document.createElement("div");
    var quizListNumber = quizContent.children.length + 1;
    quizDiv.className = `quiz${quizListNumber}`;
    quizDiv.id = `quiz${quizListNumber}`;

    quizDiv.innerHTML =
    `
        <label for="quiz" class="form-label"> quiz:</label>
        <input type=text class="form-control" name="quiz" required placeholder="quiz"/>

        </div>
        <button type="button" class="removeBtn" onclick=deletequiz(${quizListNumber})>Supprimer le quiz</button>
    `;

    quizContent.appendChild(quizDiv);
};

function deletequiz(n) {
    var quizToDelete = document.getElementById(`quiz${n}`);
    quizToDelete.remove();
};