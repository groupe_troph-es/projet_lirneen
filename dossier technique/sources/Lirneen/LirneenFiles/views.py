from flask import Flask, render_template, flash, redirect, url_for, session, request
from python.lesson_creator import create, retrieveInformations, getFromJson, retrieveLessons, addLesson, find_lesson_path, get_all_cours, get_user_cours, delLesson
from flask import Flask, render_template, flash, redirect, url_for, session, request
from python.flashcard import *
from python.flashList import *
from python.cgmnt import *
from python.quiz import *
from python.quizList import *

app = Flask(__name__)
app.config["SECRET_KEY"] = "xd!fx;hgjghxj!gbjgx,nhjgfnhbkdf dfb 3xg5154x5 2v1g4g534:h7!8d4f5"

user = None

@app.route('/register', methods=['POST','GET'])
def register():
    global user
    user=None

    #register l'utilisateur
    if request.method == "POST":
        username = request.form.get('username')
        display_name = request.form.get('display_name')
        password = request.form.get('password')
        choix = request.form.get("choix_rank")
        if not username:
            flash("Nom d'utilisateur requis!")
        elif not display_name:
            flash("Nom d'affichage requis !")
        elif not password:
            flash("Phone is required!")
        else:
            reg(username,display_name,password,choix)
            return redirect(url_for('login'))
    return render_template('register.html')

@app.route("/login",methods=["POST","GET"])
def login():
    global user
    user=None

    #login l'utilisateur
    if request.method == "POST":
        username1 = request.form.get("username")
        password1 = request.form.get("password")
        if not username1:
            flash("Nom d'utilisateur requis !",category="error")
        if not password1:
            flash("Mot de passe requis !",category="error")
        passw=log(username1)
        if password1 == passw :
            user = username1
            return redirect("/")
    return render_template("login.html")

@app.route("/",methods=["GET","POST"])
def home():
    global user
    if user==None: return redirect(url_for("login"))
    name = user

    #set up le mode jour/nuit
    infos = retrieveInformations(name)
    if "dayNightMode" not in session:
        session["dayNightMode"] = "false"

    #supprime le cour souhaité
    if request.method == "POST":
        to_del = request.form.get("to_del")
        n = 0
        for i in range(len(infos)):
            if infos[i]["Titre"] == to_del:
                n+=1
        if n > 0:
            feedback = delLesson(to_del,name)
            if feedback == "Error-UnknownOrigin": flash("Error - Un soucis est apparu.",category="error")
            return redirect("/")

    return render_template("home.html",name=get_display_name(name),infos=infos,pageName="home",dayNightMode=session["dayNightMode"],prof=est_prof(user))

@app.route("/cours",methods=["POST","GET"])
def home_cours():
    global user
    if user==None: return redirect(url_for("login"))
    name = user

    # permet de prendre les informations du cours souhaité
    if request.method == "GET":
        info = request.args.get("cours")
        print(info)
        if info != None:
            coursInfos = getFromJson(info)
            cours = retrieveLessons(info)
            print(coursInfos)
            print(cours)
            return render_template("home_cours.html",name=name,coursInfos=coursInfos,cours=cours,pageName="cours",dayNightMode=session["dayNightMode"])
        else:
            # return erreur si non trouvé
            flash("Erreur ; aucun cours trouvé.",category="error")
            return redirect("/")
    else:
        flash("Erreur ; aucun cours trouvé.",category="error")
        return redirect("/")

@app.route("/create",methods=["POST","GET"])
def cours_create():
    global user
    if user==None: return redirect(url_for("login"))
    # empêche d'accéder si pas un professeur
    if not est_prof(user):
        flash("Error - Seul les professeurs peuvent créer des cours.",category="error")
        return redirect("/")
    name = user

    #créer le cours
    if request.method == "POST":
        keys = request.form.keys()
        infos = {}
        for key in keys:
            infos[key] = request.form.get(key)
        if infos["coursDescription"] == "": infos["coursDescription"] = "Aucune Description"
        print(infos)

        feedback = create(infos)
        # si les fichiers pas dans LirneenShare
        if feedback == "error-NotFound": flash("Erreur : Des fichiers manquent dans le dossier LirneenShare.",category="error")
        elif feedback[:8]=="Success-": 
            addLesson(feedback[8:],name)
            flash("Success : Leçon correctement créée !",category="success")
            return redirect("/")

    flash("Ne choisissez des fichiers présents que dans le dossier /LirneenShare",category="info")
    return render_template("cours_create.html",dayNightMode=session["dayNightMode"])

@app.route("/lesson",methods=["GET","POST"])
def lesson():
    global user
    if user==None: return redirect(url_for("login"))
    name = user

    # permet de savoir quel leçon affichée
    if request.method=="POST":
        infos = request.form.get("goto")
        infos = infos.split(";#*f3")
        print(infos)
        print(infos[0])
        get_path = find_lesson_path(infos[0],infos[1])
        if get_path == "Error-FileNotFound":
            flash("Error : Cours non trouvé.",category="error")
        return render_template("cours_lesson.html",coursObject=get_path,pageName="cours",dayNightMode=session["dayNightMode"])

@app.route("/workshop",methods=["GET","POST"])
def workshop():
    global user
    if user==None: return redirect(url_for("login"))
    name = user

    # Permet d'ajouter un cours ou non si déjà possédé
    if request.method == "POST":
        infos = request.form.get("obtain")
        print(infos)
        userinfos = get_user_cours(name)
        n=0
        for i in range(len(userinfos)):
            if userinfos[i][1] == infos:
                n+=1
        if n>0:
            flash("Error : Vous possédez déjà ce cours.",category="error")
        else:
            addLesson(infos,name)
            flash("Cours bien ajouté !",category="success")

    CoursInfos = get_all_cours()
    infos = []
    print(CoursInfos)
    for cour in CoursInfos: infos.append(getFromJson(cour[1]))
    UserCours = get_user_cours(name)

    return render_template("workshop.html",infos=infos,UserCours=UserCours,pageName="workshop",dayNightMode=session["dayNightMode"])


##### ---- FLASHCARDS ---- #####


nom_flashcard = ""
file_ordre = File()

@app.route("/flashcard", methods=['GET','POST'])
def flashcard_menu():
    global nom_flashcard,user
    if user==None: return redirect(url_for("login"))
    name = user
    nom_flashcard= ""
    infos = None
    keys = request.form.keys()
    for key in keys:
        infos = request.form.get(key)
    if infos != None :
        ajout_flash(infos,name)
    return(render_template("flashcard_menu.html",pageName="flashcard",nom_list=list_nom(user)))

@app.route("/flashcard/creation", methods=['POST',"GET"])
def flashcard():
    global nom_flashcard,user
    if user==None: return redirect(url_for("login"))
    name = user
    if request.method == "POST":
        keys = request.form.keys()
        infos = {}
        for key in keys:
            infos[key] = request.form.get(key)
        creat_flash(infos,nom_flashcard,user)
        global file_ordre
        file_ordre = melange(nom_flashcard,name)
    return render_template("flashcard.html",pageName="flashcard")

@app.route("/flashcard/apprendre" ,methods = ['GET'])
def flashcard2():
    global nom_flashcard,user
    if user==None: return redirect(url_for("login"))
    name = user
    if est_vide(nom_flashcard) or file_ordre.estVide():
        q_r=("il n'y a plus de question","bien joué !")
    else:
        q_r=aff_fl(file_ordre,nom_flashcard,user)
    print(q_r)
    print(file_ordre)
    return render_template("flashcard2.html",pageName="flashcard",question=str(q_r[0]),reponse=str(q_r[1]))


##### ---- Ajax ---- #####


@app.route("/flashcard/reussi" , methods = ['POST'])
def reussi():
    file_ordre.enlever()
    return ('',204)

@app.route("/flashcard/reload" , methods = ['POST'])
def reload():
    global file_ordre, nom_flashcard
    file_ordre= melange(nom_flashcard, user)
    return('',204)

@app.route("/flashcard/suppr" , methods = ['POST'])
def suppr():
    global file_ordre, nom_flashcard
    sup=file_ordre.dernier()
    file_ordre.enlever()
    effacer(sup,user)
    return('',204)


@app.route("/flashcard/change_nom" , methods = ['POST'])
def change_nom():
    global nom_flashcard
    nom_flashcard=str(request.form.get('nom_flash'))
    print(nom_flashcard)
    return('',204)


@app.route("/flashcard/effacerFlash" , methods = ['POST'])
def effacerFlash():
    global nom_flashcard, user
    if user==None: return redirect(url_for("login"))
    name = user
    nom_flashcard=str(request.form.get('effacer_nom'))
    effacer_flash(nom_flashcard,name)
    effacer_fl(nom_flashcard,name)
    return('',204)


##### ---- Quiz ---- #####


quizList=""

@app.route('/quizzcreate',methods=["POST","GET"])
def quizzcreate():
    global quizList, user
    if user==None: return redirect(url_for("login"))
    name = user
    if request.method == "POST":
        contenu_question = request.form.get("question1")
        contenu_reponse = request.form.get("answer1")
        contenu_reponse2 = request.form.get("answer2")
        contenu_reponse3 = request.form.get("answer3")
        contenu_reponse4 = request.form.get("answer4")
        bonne_reponse = request.form.get("br")
        theme=quizList
        ajout_quiz(theme,contenu_question,contenu_reponse,contenu_reponse2,contenu_reponse3,contenu_reponse4,bonne_reponse)
        

    return render_template("quiz_create.html",pageName="quiz")

@app.route('/quizaffichage', methods = ["POST","GET"])
def quiz_aff():
    global quizList, user
    if user==None: return redirect(url_for("login"))
    name = user
    if request.method == "POST":
        nom_quiz = request.form.get("quiz")
        ajout_quizList(nom_quiz)


    return render_template("quizz_aff.html",pageName="quiz",quizlist=list_quiz())

@app.route('/quiztest', methods = ["POST","GET"])
def quiz_rep():
    global quizList, user
    if user==None: return redirect(url_for("login"))
    name = user
    txt_note=""
    if request.method== "POST":
        tableau=tab_quiz(quizList)
        reponse_user=[]
        for i in range(len(tableau)):
            reponse_user.append(request.form.get(str(tableau[i][5])))
        reponse = reponse_quiz(quizList)
        note=0
        taille=len(reponse)
        for i in range(taille):
            if int(reponse[i])==int(reponse_user[i]):
                note = note + 1
        txt_note="Votre note :"+str(note)+"/"+str(taille)
    return render_template("quiz.html",pageName="quiz",tab_quiz=tab_quiz(quizList),note=txt_note)


##### ---- Ajax Quiz ---- #####
@app.route("/quizaffichage/quizlist" , methods = ['POST'])
def quizlist():
    global quizList
    quizList=str(request.form.get('quizlist'))
    quizList = quizList.split("\n")[4].strip()
    print(quizList)
    return('',204)



app.run()