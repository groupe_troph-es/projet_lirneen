let sideBarBtn = document.getElementById("sideBarBtn");
var r = document.querySelector(":root");
var modColorNight = ["#49525A","#fff","aliceblue","black"];
var modColorDay = ["#fff","aliceblue","#343a40ce","#212529"];

if (sideBarBtn) {
    sideBarBtn.addEventListener("click",function() {
        var icon = this.querySelector('i');
        this.classList.toggle("rotate");
    });
};

function NightMode() {
    var rv = getComputedStyle(r);

    var currentMode = sessionStorage.getItem("dayNightMode")
    var newMode;

    if (currentMode === "false") {
        newMode = "true";
        r.style.setProperty("--main-color-background",modColorNight[0]);
        r.style.setProperty("--main-color-label-border",modColorNight[2]);
        r.style.setProperty("--main-color-font",modColorNight[2]);
    } else {
        newMode = "false";
        r.style.setProperty("--main-color-background",modColorDay[0]);
        r.style.setProperty("--main-color-label-border",modColorDay[2]);
        r.style.setProperty("--main-color-font",modColorDay[3]);
    };
    sessionStorage.setItem("dayNightMode",newMode);
};

function setNight() {
    var currentMode = sessionStorage.getItem("dayNightMode");

    if (currentMode === "true" ) {
        r.style.setProperty("--main-color-background",modColorNight[0]);
        r.style.setProperty("--main-color-label-border",modColorNight[2]);
        r.style.setProperty("--main-color-font",modColorNight[2]);
    } else {
        r.style.setProperty("--main-color-background",modColorDay[0]);
        r.style.setProperty("--main-color-label-border",modColorDay[2]);
        r.style.setProperty("--main-color-font",modColorDay[3]);
    };
};

setNight()

// #49525A