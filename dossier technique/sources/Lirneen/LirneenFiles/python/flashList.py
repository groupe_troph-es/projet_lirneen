from flask import Flask, render_template, flash, redirect, url_for, session, request
import sqlite3

def open():
    conn = sqlite3.connect("LirneenFiles/databases/flashList.sql" , check_same_thread=False)
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS flashList(id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE , nom TEXT, utilisateur TEXT);''')
    return conn





def list_nom(user):
    conn = open()
    c = conn.cursor()
    sql = ("""SELECT nom FROM flashList WHERE utilisateur = ?""")
    add = [user]
    c.execute(sql,add)
    result = c.fetchall()
    liste=[]
    for x in result:
        liste=liste + [x[0]]
    conn.close()
    return liste

def ajout_flash(nom, user):
    conn = open()
    c = conn.cursor()
    exist=[]
    sql="SELECT nom FROM flashlist WHERE utilisateur = ?"
    val=[user]
    c.execute(sql,val)
    result=c.fetchall()
    for x in result :
        exist.append(x[0])
    for ele in exist :
        if ele==nom:
            return flash("Erreur ; ce nom existe déjà",category="error")
    sql="INSERT INTO flashList (nom ,utilisateur) VALUES(?,?)"
    val=[nom, user]
    c.execute(sql,val)
    conn.commit()
    conn.close()

def effacer_fl(nom,user):
    conn = open()
    c = conn.cursor()
    sql = """DELETE FROM flashList Where nom = ? AND utilisateur = ?"""
    val = [nom, user]
    c.execute(sql,val)
    conn.commit()
    conn.close()




