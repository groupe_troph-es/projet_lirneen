import os
import zipfile
import json
import sqlite3
import shutil



def correctRename(name):
    # change le name pour être valide
    name = name.replace(" ","_")
    name = name.replace("-","_")
    replaceIt = ["'","~","#","`","^","¨",",","?",";","/",":","§","!"]
    for thatOne in replaceIt:
        name = name.replace(thatOne,"")
    eValue = ["é","è","ê","ë"]
    for thatOne in eValue:
        name = name.replace(thatOne,"e")
    aValue = ["à","â","ä"]
    for thatOne in aValue:
        name = name.replace(thatOne,"a")
    oValue = ["ô","ö"]
    for thatOne in oValue:
        name = name.replace(thatOne,"o")
    uValue = ["û","ü"]
    for thatOne in uValue:
        name = name.replace(thatOne,"u")
    iValue = ["î","ï"]
    for thatOne in iValue:
        name = name.replace(thatOne,"i")
    name = name.replace("___","_")
    name = name.replace("__","_")
    return name

def create(infos):

    # vérifie que les leçons sont bien présentes dans le dossier LirneenShare
    n=1
    lessonfileNames = "lesson"+str(n)
    while lessonfileNames in infos.keys():
        if os.path.exists("LirneenShare/"+str(infos[lessonfileNames+"File"])):
            print("Cours Trouvé...")
        else:
            return "error-NotFound"
        n += 1
        lessonfileNames = "lesson"+str(n)

    # vérifie que le nom n'existe pas pour un autre cours, sinon ajout d'un nombre
    n = 1
    conn = sqlite3.connect("LirneenFiles/databases/Cours.sql") 
    c = conn.cursor()
    results = c.execute('''SELECT * FROM CoursListe WHERE coursName == "'''+infos["coursTitre"]+'''"''')
    if len(results.fetchall()) >= 1:
        while len(results.fetchall()) >= 1:
            infos["coursTitre"] = infos["coursTitre"] + str(n)
            n+=1
            results = c.execute('''SELECT * FROM CoursListe WHERE coursName == "'''+infos["coursTitre"]+'''"''')
    print("nom trouvé!",infos["coursTitre"])
    conn.close()

    # créer les dossiers demandés
    dirName = "Lirneen-"+infos["coursTitre"]
    path = os.path.join("LirneenFiles/cours/",dirName)
    os.mkdir(path)
    fileDirName = "LirneenFiles/cours/"+dirName+"/"
    Filepath = os.path.join(fileDirName,"lesson")
    os.mkdir(Filepath)
    os.mkdir("LirneenFiles/static/pdf/"+dirName+"-lessons")

    # colle les leçons dans les dossiers demandés
    n=1
    lessonfileNames = "lesson"+str(n)
    while lessonfileNames in infos.keys():
        if os.path.exists("LirneenShare/"+str(infos[lessonfileNames+"File"])):
            print("ok")
            new_name= correctRename(infos[lessonfileNames+"File"])
            shutil.copy("LirneenShare/"+str(infos[lessonfileNames+"File"]),fileDirName+"/lesson/"+new_name)
            shutil.copy("LirneenShare/"+str(infos[lessonfileNames+"File"]),"LirneenFiles/static/pdf/"+dirName+"-lessons/"+new_name)
        else:
            return "error-NotFound"
        n += 1
        lessonfileNames = "lesson"+str(n)


    # créer la database
    file = open(str(path)+"/Lirneen-Cours.sql","w")
    file.close()

    # rentre les données dans la db
    conn = sqlite3.connect("LirneenFiles/cours/"+dirName+"/Lirneen-Cours.sql") 
    c = conn.cursor()
    c.execute('''
          CREATE TABLE IF NOT EXISTS Lessons
          ([id] INTEGER PRIMARY KEY, 
          [lesson] TEXT,
          [file] TEXT
          )
          ''')
    conn.commit()
    n=1
    lessonName = "lesson"+str(n)
    sql = """INSERT INTO Lessons(lesson,file) Values(?,?)"""
    while lessonName in infos.keys():
        print(str(correctRename(infos[str(lessonName)+"File"])))
        add = (str(infos[lessonName]),str(correctRename(infos[str(lessonName)+"File"])))
        c.execute(sql,add)
        conn.commit()
        n+=1
        lessonName = "lesson"+str(n)

    # met les infos restantes dans un json
    file_infos = {
        "Titre":infos["coursTitre"],
        "Description":infos["coursDescription"],
        "Nb_Cours":n-1}
    to_json = json.dumps(file_infos,indent=4)
    with open("LirneenFiles/cours/"+dirName+"/infos.json","w") as fw:
        fw.write(to_json)
    
    conn.close()

    # Enregistre le code dans la db général
    print("Log du cours dans les databases...")
    conn = sqlite3.connect("LirneenFiles/databases/Cours.sql")
    c = conn.cursor()
    sql = """INSERT INTO CoursListe(coursName) Values(?)"""
    add = (infos["coursTitre"],)
    c.execute(sql,add)
    conn.commit()
    conn.close()
    print("Done!")
    return "Success-"+infos["coursTitre"]

def getFromJson(coursName):
    # obtient les informations depuis json
    dirName = "LirneenFiles/cours/Lirneen-"+coursName+"/infos.json"
    file = open(dirName)
    output = json.load(file)
    file.close()
    return output

def retrieveInformations(username):
    # récupère les informations du cours
    tableName = str(username)+"Cours"
    try:
        conn = sqlite3.connect("LirneenFiles/databases/UserCours.sql")
        c = conn.cursor()
        res = c.execute("""SELECT * FROM """+tableName)
        files = res.fetchall()
        output = []
        for i in range(len(files)):
            output.append(getFromJson(files[i][1]))
        conn.close()
        return output
    except Exception as e:
        print(e)
        
def retrieveLessons(coursName):
    # récupère les leçons du cours
    try:
        conn = sqlite3.connect("LirneenFiles/cours/Lirneen-"+coursName+"/Lirneen-Cours.sql")
        c = conn.cursor()
        res = c.execute("""Select * FROM Lessons""")
        output = res.fetchall()
        conn.close()
        return output
    except Exception as e:
        print(e)
        return "Error-UnknownOrigin"

def addLesson(coursName,user):
    # ajoute un cours dans la db de l'utilisateur
    try:
        conn = sqlite3.connect("LirneenFiles/databases/UserCours.sql")
        c = conn.cursor()
        sql = """INSERT INTO """+user+"""Cours(cours) Values(?)"""
        add = (coursName,)
        c.execute(sql,add)
        conn.commit()
        conn.close()
    except Exception as e:
        print(e)
        return "Error-UnknownOrigin"

def delLesson(coursName,user):
    # supprime un cours de la db de l'utilisateur
    try:
        conn = sqlite3.connect("LirneenFiles/databases/Usercours.sql")
        c = conn.cursor()
        sql = """DELETE FROM """+user+"""Cours WHERE cours='"""+str(coursName)+"""'"""
        c.execute(sql)
        conn.commit()
        conn.close()
    except Exception as e:
        print(e)
        return "Error-UnknownOrigin"

def find_lesson_path(coursName,file):
    # vérifie que le chemin existe
    path = "pdf/Lirneen-"+str(coursName)+"-lessons/"+str(file)
    print(path)
    if os.path.exists("LirneenFiles/static/"+path): 
        print("GOOD",path)
        return path
    else: return "Error-FileNotFound"

def get_all_cours():
    # obtient l'entièreté de la liste des cours
    conn = sqlite3.connect("LirneenFiles/databases/Cours.sql")
    c = conn.cursor()
    result = c.execute("""SELECT * FROM CoursListe""").fetchall()
    conn.close()
    return result

def get_user_cours(username):
    # obtient l'entièreté de la liste des cours de la db de l'utilisateur
    conn = sqlite3.connect("LirneenFiles/databases/UserCours.sql")
    c = conn.cursor()
    result = c.execute("""SELECT * FROM """+str(username)+"""Cours""").fetchall()
    conn.close()
    return result