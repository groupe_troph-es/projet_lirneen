import sqlite3
import random
def open():
    conn = sqlite3.connect("LirneenFiles/databases/flashcards.sql" , check_same_thread=False) 
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS flashcard(id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, id_fl,question TEXT,reponse TEXT, utilisateur TEXT);''')
    return conn

##---- classe File ----##

class File:
    def __init__(self):
        self.file = []

    def ajouter(self, e):
        self.file.append(e)

    def premier(self):
        return self.file[0]
    
    def enlever(self):
        self.file.pop(-1)

    def dernier(self):
        return self.file[-1]

    def prelever(self):
        s = self.file.pop(0)
        self.file.append(s)
        return s

    def estVide(self):
        return len(self.file) == 0
    
    def compter(self):
        n=len(self.file)
        return n

    def __str__(self):
        retour = ""
        for e in self.file:
            retour += str(e) + ','
        return "<- " + retour + "<-"
    
###----------SQL/fonctions----------###

def est_vide(fl):
    conn=open()
    c=conn.cursor()
    c.execute("SELECT * FROM flashcard WHERE id_fl = ?" , [fl])
    result = c.fetchall()
    liste_flash=[]
    conn.cursor()
    for x in result:
        liste_flash.append(x)
    if liste_flash==[]:
        return True
    else: 
        return False
        

def creat_flash(ditio,fl,user):
    conn=open()
    c=conn.cursor()
    sql = "INSERT INTO flashcard (question, reponse,id_fl,utilisateur) VALUES(?, ?, ?, ?)"
    val=[ditio['question'],ditio['reponse'],fl,user]
    c.execute(sql,val)
    conn.commit()
    conn.close()


def affiche(fl,user):
    conn=open()
    c=conn.cursor()
    c.execute("SELECT * FROM flashcard WHERE id_fl = ? AND utilisateur = ?" , [fl,user])
    result = c.fetchall()
    for x in result:
        print(x)
    conn.close()

def effacer(ident,user):
    conn=open()
    c=conn.cursor()
    sql = """DELETE FROM flashcard Where id = ? AND utilisateur = ?"""
    val = [ident,user]
    c.execute(sql,val)
    conn.commit()
    conn.close()

def effacer_flash(fl,user):
    conn=open()
    c=conn.cursor()
    sql = """DELETE FROM flashcard Where id_fl = ? AND utilisateur = ?"""
    val = [fl,user]
    c.execute(sql,val)
    conn.commit()
    conn.close()

def melange(fl,user):
    conn=open()
    c=conn.cursor()
    fl_temp=[]
    file= File()
    sql="SELECT id FROM flashcard WHERE id_fl= ? AND utilisateur = ? "
    add=[fl,user]
    c.execute(sql,add)
    result = c.fetchall()
    for x in result:
        fl_temp.append(int(x[0]))
    fl_temp=random.sample(fl_temp, k=len(fl_temp))
    for ele in fl_temp:
        file.ajouter(ele)
    conn.close()
    return file 

def aff_fl(file,fl,user):
    conn=open()
    c=conn.cursor()
    assert type(file) == File , "le parametre doit être une file"
    ques_rep=[]

    sql1="SELECT question FROM flashcard WHERE id=? AND id_fl = ? AND utilisateur = ?"
    sql2="SELECT reponse FROM flashcard WHERE id=? AND id_fl = ? AND utilisateur = ?"
    val1=[file.prelever(),fl,user]
    c.execute(sql1,val1)
    result = c.fetchall()
    for x in result:
        ques_rep.append(x[0])
    c.execute(sql2,val1)
    result = c.fetchall()
    for x in result:
        ques_rep.append(x[0])
    conn.close()
    return ques_rep
    
