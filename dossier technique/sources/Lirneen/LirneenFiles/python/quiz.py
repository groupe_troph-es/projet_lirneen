import sqlite3
def open():
    conn = sqlite3.connect("LirneenFiles/databases/quiz.sql" , check_same_thread=False) 
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS quiz(id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, id_quiz,question TEXT ,R1 TEXT,R2 TEXT,R3 TEXT,R4 TEXT, BR INT);''')
    return conn

def ajout_quiz(theme,quest,r1,r2,r3,r4,br):
    conn = open()
    c = conn.cursor()
    sql="INSERT INTO quiz (id_quiz,question,R1,R2,R3,R4,BR) VALUES(?, ?, ?, ?, ?, ?, ?)"
    add = [theme,quest,r1,r2,r3,r4,br]
    c.execute(sql,add)
    conn.commit()
    conn.close()

def del_quiz(ident):
    conn=open()
    c=conn.cursor()
    sql = """DELETE FROM quiz Where id = ? """
    val = [ident]
    c.execute(sql,val)
    conn.commit()
    conn.close()

def tab_quiz(quizlist):
    conn = open()
    c = conn.cursor()
    sql="""SELECT question, R1, R2, R3, R4 ,id FROM quiz WHERE id_quiz = ? """
    c.execute(sql,[quizlist])
    result = c.fetchall()
    tab=[]
    for x in result:
        tab.append(list(x))
    conn.close()
    return tab






def affiche_quiz(idq):
    conn=open()
    c=conn.cursor()
    sql = ("SELECT * FROM quiz WHERE id = ? ")
    add = [idq]
    c.execute( sql,add)
    result = c.fetchall()
    for x in result:
        print(x)
    conn.close()


def reponse_quiz(quizList):
    conn=open()
    c=conn.cursor()
    sql="SELECT BR FROM quiz WHERE id_quiz = ?"
    c.execute(sql,[quizList])
    result = c.fetchall()
    conn.close()
    reponses=[]
    for x in result:
        reponses.append(x[0])
    return reponses

    
    
        