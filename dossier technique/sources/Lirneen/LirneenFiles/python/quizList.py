import sqlite3

def open():
    conn = sqlite3.connect("LirneenFiles/databases/quizList.sql" , check_same_thread=False)
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS quizList(id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE , id_quiz TEXT);''')
    return conn

def ajout_quizList(nom):
    conn = open()
    c = conn.cursor()
    sql="INSERT INTO quizList (id_quiz ) VALUES(?)"
    val=[nom]
    c.execute(sql,val)
    conn.commit()
    conn.close()


def effacer_quiz(nom):
    conn = open()
    c = conn.cursor()
    sql = """DELETE FROM quizList Where id_quiz = ? """
    val = [nom]
    c.execute(sql,val)
    conn.commit()
    conn.close()

liste=[]
def list_quiz():
    conn = open()
    c = conn.cursor()
    sql="""SELECT id_quiz FROM quizList """
    c.execute(sql)
    result = c.fetchall()
    liste=[]
    for x in result:
        liste.append(x[0])
    conn.close()
    return liste
