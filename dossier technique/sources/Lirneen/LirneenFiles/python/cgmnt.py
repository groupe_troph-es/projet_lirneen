import sqlite3
from flask import Flask, render_template, flash, redirect, url_for, session, request

def open():
    # ouvre la base de donnée
    conn = sqlite3.connect('LirneenFiles/databases/users.sql')
    c = conn.cursor()
    c.execute('''
        CREATE TABLE IF NOT EXISTS users (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            username TEXT NOT NULL,
            display_name TEXT NOT NULL,
            password TEXT NOT NULL,
            rank TEXT NOT NULL
            )
        ''')
    return conn


def reg(username,display_name,password,choix):
    conn=open()
    c = conn.cursor()
    exist=[]
    sql="SELECT username FROM users"
    c.execute(sql)
    result=c.fetchall()
    for x in result :
        exist.append(x[0])
    for ele in exist :
        if ele==username:
            return flash("Erreur ; ce nom existe déjà",category="error")
    sql = """INSERT INTO users (username, display_name, password, rank) VALUES(?, ?, ?, ?)"""
    add = (username, display_name, password, choix)
    c.execute (sql,add)
    conn.commit()
    conn.close()
    conn = sqlite3.connect("LirneenFiles/databases/UserCours.sql") 
    c = conn.cursor()
    c.execute('''
        CREATE TABLE IF NOT EXISTS '''+username+'''Cours
        ([id] INTEGER PRIMARY KEY, 
        [cours] TEXT
        )
        ''')

def log(username1):
    conn=open()
    c = conn.cursor()
    sql=("SELECT password From users WHERE username = ?")
    add=[username1]
    c.execute(sql,add)
    result=c.fetchall()
    if len(result) ==0:
         passw= None
    for x in result :
        passw=x
    if passw == None :
         return None
    return passw[0]




def est_prof(username):
    conn=open()
    c=conn.cursor()
    sql="select rank from users WHERE username = ?"
    add=[username]
    c.execute(sql,add)
    result=c.fetchall()
    if result[0][0]== "PROF":
        return True
    else : 
        return False
    
def get_display_name(username):
    conn=open()
    c=conn.cursor()
    sql="select display_name from users WHERE username = ?"
    add=[username]
    c.execute(sql,add)
    result=c.fetchall()
    return result[0][0]

print(get_display_name("Eryx"))


