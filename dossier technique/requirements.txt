Pour lancer le programme, Windows est conseillé mais Linux fonctionne également, bien que les derniers tests aient été fait exclusivement sur Windows

Lancer Visual Studio Code
Ouvrir le dossier " Lirneen " dans le répertoire " sources " afin de maintenir les chemins corrects
Aller sur le fichier " views.py " et lancer le programme

Aller sur un navigateur ( Chrome conseillé )
Ecrire dans l'adresse : " localhost:5000 "
Le site devrait vous renvoyer sur la page de login 