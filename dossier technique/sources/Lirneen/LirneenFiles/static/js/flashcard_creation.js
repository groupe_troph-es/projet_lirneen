let flashNumber = 0

function addflash() {
    var flashcontent = document.getElementById("flashcontent");
    var flashDiv = document.createElement("div");
    flashNumber = flashcontent.children.length + 1;
    flashDiv.className = `flash${flashNumber}`;
    flashDiv.id = `flash${flashNumber}`;

    flashDiv.innerHTML =
    `
        <label for="flash" class="form-label"> flashcard:</label>
        <input type=text class="form-control" name="flash" required placeholder="flashcard"/>
        <button type="button" class="deleat" onclick=deleteflash(${flashNumber})>Supprimer la flashcard</button>
    `;

    flashcontent.appendChild(flashDiv);
};

function deleteflash(n) {
    var flashToDelete = document.getElementById(`flash${n}`);
    flashToDelete.remove();
};

if ( window.history.replaceState ) {
    window.history.replaceState( null, null, window.location.href );
}