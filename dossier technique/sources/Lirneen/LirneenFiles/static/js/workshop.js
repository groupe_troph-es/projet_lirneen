function searchFunction() {
    var input = document.getElementById("InputSearch");
    var filter = input.value.toUpperCase();
    var div = document.querySelector(".CoursCards");
    var coursTitles = div.getElementsByClassName("card-title");
    var titleText, i;

    for (i=0; i<coursTitles.length; i++) {
        titleText = coursTitles[i].textContent || coursTitles[i].innerText;

        if (titleText.toUpperCase().indexOf(filter) > -1) {
            coursTitles[i].parentNode.parentNode.parentNode.parentNode.style.display = "";
        } else {
            coursTitles[i].parentNode.parentNode.parentNode.parentNode.style.display = "none";
        }
    }
};