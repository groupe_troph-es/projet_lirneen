function launchPopupHome() {
    var popup = document.getElementById("popupHome");
    popup.classList.toggle("open-popup");
};

function launchPopupSubmit() {
    var popup = document.getElementById("popupSubmit");
    popup.classList.toggle("open-popup");
};

function closePopup() {
    var popup = document.getElementById("popupSubmit");
    popup.classList.toggle("open-popup");
};

function goToCours() {
    window.location.href = "/";
};

function addLesson() {
    var lessonContent = document.getElementById("lessonContent");
    var lessonDiv = document.createElement("div");
    var lessonNumber = lessonContent.children.length + 1;
    lessonDiv.className = `lesson${lessonNumber}`;
    lessonDiv.id = `lesson${lessonNumber}`;

    lessonDiv.innerHTML =
    `
        <label for="lesson${lessonNumber}" class="form-label"> Leçon ${lessonNumber}:</label>
        <input type=text class="form-control" name="lesson${lessonNumber}" required placeholder="Leçon ${lessonNumber}"/>
        <input type=file class="form-control" name="lesson${lessonNumber}File" required accept=".pdf,.html,.md,image/*,audio/*,video/*"/>
        <!--<button type="button" onclick=addExercice(${lessonNumber})>Ajouter Un Exercice</button>
        <div id="exerciceContent${lessonNumber}">

        </div>!-->
        <button type="button" class="removeBtn" onclick=deleteLesson(${lessonNumber})>Supprimer la leçon</button>
    `;

    lessonContent.appendChild(lessonDiv);
};

function deleteLesson(n) {
    var lessonToDelete = document.getElementById(`lesson${n}`);
    lessonToDelete.remove();
};



document.addEventListener("DOMContentLoaded",function() {
    const formControlInputs = document.querySelectorAll(".form-control");
    const confirmButton = document.querySelector(".confirm");

    confirmButton.disabled = true;

    formControlInputs.forEach(input => {
        input.addEventListener("input",function() {
            let allFilled = true;
            formControlInputs.forEach(input => {
                if (input.value.trim() === '' && !input.classList.contains("desc")) {
                    allFilled = false;
                };
            });
            confirmButton.disabled = !allFilled
        });
    });
    confirmButton.addEventListener("click",function(event) {
        if (confirmButton.disabled) {
            event.preventDefault();
            alert("Veuillez remplir toutes les entrées.")
        };
    });
});









function addExercice(n) {
    var exerciceContent = document.getElementById(`exerciceContent${n}`);
    //alert(exerciceContent)
    var exerciceDiv = document.createElement("div");
    exerciceDiv.className = "exercice";
    var exerciceNumber = exerciceContent.children.length + 1;

    exerciceDiv.innerHTML =
    `
    <label for="exercice${exerciceNumber}"> Exercice ${exerciceNumber}:</label>
    <input type=text name="exercice${exerciceNumber}Name" required placeholder="Votre Question">
    <select id="exercice${exerciceNumber}" required>
        <option value="freeAnswer">Free Answer</option>
        <!--<option value="match">Match</option>!-->
        <option value="fixed">Fixed Answers</option>
    </select>
    <input type=text name="exercice${exerciceNumber}" required placeholder="Bonne Réponse">
    <input type=text name=""exercice${exerciceNumber}" required placeholder="Mauvaise Réponse 1">
    <input type=text name=""exercice${exerciceNumber}" required placeholder="Mauvaise Réponse 2">
    <input type=text name=""exercice${exerciceNumber}" required placeholder="Mauvaise Réponse 3">
    `;

    exerciceContent.appendChild(exerciceDiv);
};